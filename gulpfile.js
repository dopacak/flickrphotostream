var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: ''
    }
  });
});


gulp.task('sass', function() {
  return gulp.src('scss/**/*.scss') // Gets all files ending with .scss in app/scss
      .pipe(sass().on('error', function (err) {
        sass.logError(err);
      }))
    .pipe(autoprefixer())
    .pipe(gulp.dest('css'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('watch', ['browserSync'], function (){
  gulp.watch('scss/**/*.scss', ['sass']);
  gulp.watch('js/**/*.js', browserSync.reload);
  gulp.watch('index.html', browserSync.reload);
});
