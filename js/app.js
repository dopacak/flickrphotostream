window.fps = { app: {} }; // flickr photo stream

// Data
(function(fps, _, window, document, undefined) {
    function Data() {
        this.photos = [];
        this.photosMap = {};
        this.subscriptions = {};
        this.jsonFlickrFeed = this._jsonFlickrFeed.bind(this);
        this.fetch = _.debounce(this._fetch, 200);
    }

    Data.prototype.subscribe = function(key, callback, scope) {
        if (!key || !callback || this.subscriptions[key]) {
            return;
        }
        this.subscriptions[key] = { callback: callback, scope: scope };
    };

    Data.prototype.unsubscribe = function(key) {
        if (!key) {
            return;
        }
        delete this.subscriptions[key];
    };

    Data.prototype._jsonFlickrFeed = function(doc) {
        if (!doc || !doc.items || !doc.items.length) {
            return;
        }
        var items = doc.items;
        var item;
        var photos = this.photos;
        var photosMap = this.photosMap;
        var photosAdded = [];

        for (var i = 0; i < items.length; i++) {
            item = items[i];
            if (photosMap[item.link] !== undefined) {
                continue;
            }
            item.descriptionShort = item.description.replace(item.media.m, '');
            photosAdded.push(item);
            photos.push(item);
            photosMap[item.link] = i;
        }

        var result = {
            photos: photos,
            photosAdded: photosAdded
        };
        var subscriptions = this.subscriptions;
        var subscription;
        for (var key in subscriptions) {
            if (subscriptions.hasOwnProperty(key)) {
                subscription = subscriptions[key];
                subscription.callback.call(subscription.scope, result);
            }
        }
    };

    Data.prototype._fetch = function fetch() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'http://api.flickr.com/services/feeds/photos_public.gne?format=json';
        document.body.appendChild(script);
    };

    fps.data = new Data();
    window.jsonFlickrFeed = fps.data.jsonFlickrFeed;
})(window.fps, window._, window, document);

// List
(function(fps, _, Blazy, window, document, undefined) {
    function List(data) {
        this.photos = [];
        this.photosMap = {};
        this.template = _.template(document.getElementById('template-flickr').text);
        this.data = data;
        data.subscribe('list', this.render, this);
        data.fetch();
        this.infiniteScroll = _.debounce(this._infiniteScroll, 100).bind(this);
        this.search = this._search.bind(this);
        window.addEventListener('scroll', this.infiniteScroll);

        this.container = document.getElementById('flickr-container');
        this.input = document.getElementById('flickr-search');
        this.input.oninput = this.search;
    }

    List.prototype.render = function(doc) {
        if (!doc || !doc.photosAdded || !doc.photosAdded.length) {
            this.search();
            return;
        }
        var photosAdded = doc.photosAdded;
        var photo;
        var template = this.template;
        var photoHtml = '';

        for (var i = 0; i < photosAdded.length; i++) {
            photo = photosAdded[i];
            photoHtml += template(photo);
        }

        document.getElementById('flickr-container').insertAdjacentHTML('beforeend', photoHtml);
        if (this.blazy) {
            this.blazy.revalidate();
        }
        else {
            this.blazy = new Blazy();
        }
        this.search();
    };

    List.prototype._infiniteScroll = function() {
        var triggerPoint = 1000;
        var el = document.body;
        var top = (document.documentElement && document.documentElement.scrollTop) ||
              document.body.scrollTop;
        if (top + window.innerHeight + triggerPoint > el.scrollHeight) {
            this.data.fetch();
        }
    };

    List.prototype._search = function() {
        var value = this.input.value;
        if (value) {
            this.container.classList.add('searching');
        }
        else {
            this.container.classList.remove('searching');
        }

        var photos = this.data.photos;
        var i;
        var result = [];
        var valueRegex = new RegExp(value, 'i');
        var photo;
        for (i = 0; i < photos.length; i++) {
            photo = photos[i];
            if (photo.title && photo.title.search(valueRegex) >= 0 ||
                photo.tags && photo.tags.search(valueRegex) >= 0) {
                result.push(photo);
            }
        }

        var elements = [].slice.call(document.getElementsByClassName('filtered'));
        for (i = 0; i < elements.length; i++) {
            var el = elements[i];
            el.classList.remove('filtered');
        }

        var photoEl;
        for (i = 0; i < result.length; i++) {
            photo = result[i];
            photoEl = document.getElementById(photo.link);
            if (!photoEl) {
                continue;
            }

            photoEl.classList.add('filtered');
        }
        this.blazy.revalidate();
        if (result.length < 30) {
            this.data.fetch();
        }
        else {
            this.infiniteScroll();
        }
    };

    fps.app.List = List;
})(window.fps, window._, window.Blazy, window, document);

// Main App
(function(fps, _, window, document, undefined) {
    var templateEl = document.getElementById('template-flickr');
    if (!templateEl || !_) {
        return;
    }
    var template = _.template(templateEl.innerHTML);
    fps.tempate = template;

    fps.list = new fps.app.List(fps.data);
})(window.fps, window._, window, document);
