# Flickr Photo Stream
This is a very basic demonstration / prototype of a Flickr Photo Stream and should be treated as such.

## Testing Only
Open index.html.

## Development Mode
1. Clone this repository
2. Install dependencies 'npm install'
3. Compile sass using 'gulp sass' or use the auto watch feature 'gulp watch'
